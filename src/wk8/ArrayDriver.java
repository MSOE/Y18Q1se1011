package wk8;

public class ArrayDriver {
    public static void main(String[] args) {
        String[] words = new String[102];
        words[0] = "key";
        words[1] = "pedal";
        words[2] = "string";
        words[3] = "piano";
        words[4] = "sharp";
        words[5] = "flat";
        words[6] = "tune";
        for(int i=0; i<words.length && words[i]!=null; ++i) {
                System.out.print(words[i].length() + " ");
        }

    }

    public static void main2(String[] args) {
        int[] data;
        data = new int[5];
        data[0] = 76;
        data[4] = -21;
        data = new int[10];
        for(int i=0; i<data.length; ++i) {
            System.out.print(data[i] + " ");
        }
    }
}
