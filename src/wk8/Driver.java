package wk8;

import java.util.Scanner;

public class Driver {
    public static void main(String args[]) {
        int again = 1;
        Scanner play = new Scanner(System.in);
        while(again == 1) {
            double fixedAmount = (Math.random() * 5000);
            int choice = askUserForChoice(fixedAmount);
            int weeksAlive = (int) (Math.random() * 40);
            displayWeeklyPayouts(fixedAmount, weeksAlive);
            double fixedPayoutTotal = 0;
            double exponentialPayoutTotal = 0;
            double decision = 0;
            if (choice == 1) {
                fixedPayoutTotal = fixedAmount * weeksAlive;
                exponentialPayoutTotal = Math.pow(2, weeksAlive - 1) * .01;
                decision = fixedPayoutTotal - exponentialPayoutTotal;
            }
            if (choice == 2) {
                fixedPayoutTotal = (Math.pow(2, weeksAlive - 1) * .01);
                exponentialPayoutTotal = fixedAmount * weeksAlive;
                decision = fixedPayoutTotal - exponentialPayoutTotal;
            }
            if (decision > 0) {
                System.out.println("Because you chose Option " + choice + ", your uncle will give you" +
                        " a total of $" + String.format("%.2f", fixedPayoutTotal) + ". If you would have taken the other option, ");
                System.out.println("you would have been given $" + String.format("%.2f", exponentialPayoutTotal) + ". You got lucky and ended up with an extra $" + String.format("%.2f", decision));
            } else {
                System.out.println("Because you chose Option " + choice + ", your uncle will give you" +
                        " a total of $" + String.format("%.2f", fixedPayoutTotal) + ". If you would have taken the other option, ");
                System.out.println("you would have been given $" + String.format("%.2f", exponentialPayoutTotal) + ". You got unlucky and missed out on $" + String.format("%.2f", (-1) * decision));
            }
            System.out.println();
            System.out.println();
            System.out.println();
            System.out.println("Would you like to play again? Enter '1' for yes, any other integer to exit.");
            again = play.nextInt();
        }
    }

    /**
     * Displays the payouts for each method for all weeks up to the exponential
     * method being larger than the fixed method or your uncle dying.
     * @param fixedAmount The incremental amount added to the payout in the fixed amount case
     * @param weeksAlive How long your uncle lives
     */
    private static void displayWeeklyPayouts(double fixedAmount, int weeksAlive) {
        int week = 1;
        while (week <= weeksAlive && (fixedAmount*(week+1)) > Math.pow(2, week * .01 - 1)) {
            System.out.println("Week " + week + ": Option 1 gives $" + String.format("%.2f", week*fixedAmount) +" and Option 2 gives $" + Math.pow(2, week - 1) * .01);
            ++week;
        }
        System.out.println();
        System.out.println("Your uncle dies after " + weeksAlive + " weeks.");
    }

    private static int askUserForChoice(double weekly) {
        System.out.println();
        System.out.println("Lets play a game.");
        System.out.println();
        System.out.println("Your uncle is rich and he wants to help you pay " +
                "for college. There are two options.");

        System.out.println("Option 1: He will give you $" + String.format("%.2f", weekly) + " per week. Pretty simple.");
        System.out.println("Option 2: He starts you off with a single penny, but he doubles " +
                "the value every week.");
        System.out.println("However, here's the catch: He has less than 40 weeks to live, " +
                "and when he dies he cannot give you any more money.");
        System.out.println("So. Which will it be, Option 1 or 2?");
        Scanner option = new Scanner(System.in);
        return option.nextInt();
    }
}
