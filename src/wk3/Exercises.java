package wk3;

import javax.swing.*;

public class Exercises {
    /**
     * Write a program that shows a pop-up box asking the user to enter
     * something. If the user clicks OK without entering anything, the
     * program should ask the user again (repeatedly, until the user
     * enters something). The program should then end. It is okay for
     * the program to crash if the user clicks Cancel
     * Optionally, you could modify your program to end if the user
     * clicks Cancel (hint: if the String returned from the
     * showInputDialog() call is null).
     * @param args ignored
     */
    public static void main(String[] args) {
        String userResponse;
        do {
            userResponse = JOptionPane.showInputDialog(null,
                    "Enter a string");
        } while(userResponse.equals(""));
    }
}




