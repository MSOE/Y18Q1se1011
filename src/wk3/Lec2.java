package wk3;

import java.util.Scanner;

/*
review time
code repo
submitting
 */
/*
if (CONDITIONAL) {
    Runs if CONDITIONAL is true
}

if (CONDITIONAL) {
    Runs if CONDITIONAL is true
} else {
    Runs if CONDITIONAL is false
}

if (CONDITIONAL) {
    Runs if CONDITIONAL is true
} else if (CONDITIONAL_B) {
    Runs if CONDITIONAL is false and CONDITIONAL_B is true
} else {
    Runs if CONDITIONAL is false and CONDITIONAL_B is false
}

switch (VALUE) {
    case MATCH1:
        Runs if VALUE matches MATCH1
        break;
    case MATCH2:
        Runs if VALUE matches MATCH2
        break;
    default:
        Runs if no matches were found
}

while (CONDITIONAL) {
    Runs as long as CONDITIONAL is true
}

do {
    Runs as long as CONDITIONAL is true
} while (CONDITIONAL);
 */
public class Lec2 {

    public static void main(String[] args) {
        int i = -20;
        do {
            System.out.println(i);
            i--;
            i--;
        } while (i>0);
        System.out.println(i);
    }

    public static void main3(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Enter a letter grade");
        String letterGrade = in.next();
        int grade;
        switch (letterGrade) {
            case "A":
                grade = 97;
                break;
            case "AB":
                grade = 91;
                break;
            case "B":
                grade = 86;
                break;
            case "BC":
                grade = 82;
                break;
            case "C":
                grade = 78;
                break;
            case "CD":
                grade = 75;
                break;
            case "D":
                grade = 71;
                break;
            default:
                grade = 0;
        }
        System.out.println("That corresponds to " + grade + " percent.");
    }

    public static void main2(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Enter a letter grade");
        String letterGrade = in.next();
        int grade;
        if(letterGrade.equals("A")) {
            grade = 97;
        } else if(letterGrade.equals("AB")) {
            grade = 91;
        } else if(letterGrade.equals("B")) {
            grade = 86;
        } else if(letterGrade.equals("BC")) {
            grade = 82;
        } else if(letterGrade.equals("C")) {
            grade = 78;
        } else if(letterGrade.equals("CD")) {
            grade = 75;
        } else if(letterGrade.equals("D")) {
            grade = 71;
        } else {
            grade = 0;
        }
        System.out.println("That corresponds to " + grade + " percent.");
    }
}










