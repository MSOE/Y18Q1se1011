package wk3;

import javax.swing.JOptionPane;

public class Driver {
    public static void main(String[] args) {
        String word = JOptionPane.showInputDialog(null, "Enter a word");
        char firstLetter = word.charAt(0);
        char secondFiddle;
        if (Character.isUpperCase(firstLetter)) {
            JOptionPane.showMessageDialog(null,
                    "That word started with a capital letter");
            if (firstLetter == 'A' || firstLetter == 'E' ||
                    firstLetter == 'I' || firstLetter == 'O' ||
                    firstLetter == 'U' || firstLetter == 'Y') {
                secondFiddle = 'f';
                JOptionPane.showMessageDialog(null,
                        "Vowels make our language");
            }
        } else if (Character.isDigit(firstLetter)) {
            JOptionPane.showMessageDialog(null,
                    "That word started\n with a digit");
        } else {
            JOptionPane.showMessageDialog(null,
                    "That word did not start with a capital or a digit");
        }
        secondFiddle = 'e';
    }
}
