package wk7;

import java.util.Scanner;

public class Complex {
    private double real;
    private double imaginary;

    public Complex(String complexNumber) {
        Scanner parser = new Scanner(complexNumber.substring(0, complexNumber.length()-1));
        real = parser.nextDouble();
        parser.next();
        imaginary = parser.nextDouble();
    }

    public Complex(Complex that) {
        this(that.real, that.imaginary);
        //real = that.real;
        //imaginary = that.imaginary;
    }

    public Complex(double realAnswer, double imagAnswer) {
        real = realAnswer;
        imaginary = imagAnswer;
    }

    public Complex times(Complex that) {
        double realAnswer = this.real * that.real - this.imaginary * that.imaginary;
        double imagAnswer = this.real * that.imaginary + this.imaginary * that.real;

        return new Complex(realAnswer, imagAnswer);
    }

    public Complex times(double real) {
        return new Complex(this.real*real, this.imaginary*real);
    }

    public void swap(Complex that) {
        Complex otherThing = new Complex(that);
        this.real = that.real;
        this.imaginary = that.imaginary;
        that = otherThing;
        that.real = otherThing.real;
        that.imaginary = otherThing.imaginary;
    }

    public String toString() {
        return "" + real + " + " + imaginary + "i";
    }

    // 3 + 8i 1 + 0i
    public boolean equals(Object that) {
        Complex otherThing  = (Complex)that;
        return this.real==otherThing.real && this.imaginary==otherThing.imaginary;
    }
}
