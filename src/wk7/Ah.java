package wk7;


import java.util.Scanner;

public class Ah {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        //Complex answer2;

        System.out.println("Enter a complex number in the form: 3.0 + 4.3i");
        Complex c1 = new Complex(in.nextLine());

        System.out.println("Enter a complex number in the form: 3.0 + 4.3i");
        Complex c2 = new Complex(in.nextLine());

        Complex answer = c1.times(c2);
        Complex answer2 = c2.times(3.8);

        System.out.println(c1.equals(answer));
        System.out.println(c1 == answer);
        System.out.println(c1.equals("happy"));

        System.out.println(c1 + " * " + c2 + " = " + answer);
    }
}
