package wk9;

import java.util.ArrayList;

public class Driver2 {
    public static void main(String[] args) {
        ArrayList<Integer> nums = new ArrayList<>();
        nums.add(3);
        nums.add(2);
        nums.add(1);
        nums.add(0);
        int total = 0;
        for(int value : nums) {
            total += value;

        }
        for(int i=0; i<nums.size(); ++i) {
            int value = nums.get(i);
            total += value;
        }
        System.out.println((double)total/nums.size());
    }
    public static void main2(String[] args) {
        ArrayList<String> words = new ArrayList<>();
        words.add("something else");
        words.add("else");
        words.add("something");
        System.out.println(words.indexOf("else"));
    }
}
