package wk9;

import java.util.ArrayList;

public class Driver {
    public static void main(String[] args) {
        ArrayList<Integer> nums = new ArrayList<>();
        nums.add(0);
        nums.add(2);
        nums.add(3);
        nums.add(8);
        nums.add(9);
        nums.remove(2);
        System.out.println(nums);
    }
    public static void main2(String[] args) {
        ArrayList<String> words = new ArrayList<>();
        words.add("smiles");
        words.add("happy");
        words.add("monkey");
        words.add("smiles");
        double average = averageLength(words);
        System.out.println(average);
    }

    private static double averageLength(ArrayList<String> words) {
        int lengthTotal = 0;
        for(int i=0; i<words.size(); ++i) {
            String word = words.get(i);
            lengthTotal += word.length();
        }
        return (double)lengthTotal/words.size();
    }
    private static double averageLengthAwesome(ArrayList<String> words) {
        int lengthTotal = 0;
        for(String word : words) {
            lengthTotal += word.length();
        }
        return (double)lengthTotal/words.size();
    }
}
