package wk5;

public class Person {
    final static int NORMAL_RANGE_TOP = 24;
    final static int OVERWEIGHT_RANGE_TOP = 29;
    private String name;
    private int heightInInches;
    private int weightInLbs;

    public Person() {
        name = "Phil Johnson";
        heightInInches = 65;
        weightInLbs = 130;
    }

    public Person(String nameOfPerson, int height, int weight) {
        name = nameOfPerson;
        heightInInches = height;
        weightInLbs = weight;
    }

    public int getHeight() {
        return heightInInches;
    }

    public String getFirstName() {
        return name.substring(0, name.indexOf(" "));
    }

    public void setWeight(int weightInLbs) {
        this.weightInLbs = weightInLbs;
    }

    private double calculateBMI() {
        double mass = weightInLbs*0.45;
        double heightInCentimeters = heightInInches*0.025;
        return mass / (heightInCentimeters*heightInCentimeters);
    }

    public boolean isOverweight() {
        double bmi = calculateBMI();
        return bmi>NORMAL_RANGE_TOP && bmi<=OVERWEIGHT_RANGE_TOP;
    }

}











