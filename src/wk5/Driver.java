package wk5;

public class Driver {
    public static void main(String[] args) {
        Person phil = new Person();
        Person jill = new Person("Lisa Johnson", 58, 100);
        System.out.println(phil.isOverweight());
        phil.setWeight(200);
        System.out.println(Person.NORMAL_RANGE_TOP);
        System.out.println(phil.getFirstName());
        System.out.println(phil.isOverweight());
        System.out.println(jill.isOverweight());
    }
}
